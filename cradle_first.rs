// taken from http://compilers.iecc.com/crenshaw/

// to do a set number of loops you can do
// for 5.times { do_some_stuff() }

use core::io::{Reader, ReaderUtil, Writer};

pub struct Parser {
    reader: @Reader,
    writer: @Writer,
    look: char
}

impl Parser {
    fn new(reader: @Reader, writer: @Writer) -> Parser {
        Parser {
            reader: reader,
            writer: writer,
            look: ' '
        }
    }

    priv fn get_char(&mut self) {
        self.look = self.reader.read_char();
    }

    priv fn error(&mut self, s: ~str) {
        io::println(~"Error: " + s);
    }

    priv fn abort(&mut self, s: ~str) {
        self.error(s);
        fail!();
    }

    priv fn expected(&mut self, s: ~str) {
        self.abort(s + ~" expected");
    }

    priv fn match_(&mut self, x: char) {
        if self.look == x { self.get_char(); }
        else { self.expected(fmt!("\"%c\"", x)); }
    }

    priv fn is_alpha(&mut self, c: char) -> bool {
        char::is_ascii(c)
    }

    priv fn is_digit(&mut self, c: char) -> bool {
        char::is_digit(c)
    }

    priv fn is_addop(&mut self, c: char) -> bool {
        match c {
            '+' => true,
            '-' => true,
            _   => false
        }
    }

    priv fn is_mulop(&mut self, c: char) -> bool {
        match c {
            '*' => true,
            '/' => true,
            _   => false
        }
    }

    priv fn is_operator(&mut self, c: char) -> bool {
        self.is_addop(c) || self.is_mulop(c)
    }

    priv fn get_name(&mut self) -> char {
        if !self.is_alpha(self.look) { self.expected(~"Name"); }

        let name = self.look;
        self.get_char();
        name
    }

    priv fn get_num(&mut self) -> char {
        if !self.is_digit(self.look) { self.expected(~"Integer"); }

        let num = self.look;
        self.get_char();
        num
    }

    priv fn emit(&mut self, s: ~str) {
        self.writer.write_str(~"\t" + s);
    }

    priv fn emitln(&mut self, s: ~str) {
        self.writer.write_line(~"\t" + s);
    }

    priv fn ident(&mut self) {
        let name = self.get_name();
        if self.look == '(' {
            self.match_('(');
            self.match_(')');
            self.emitln(fmt!("BSR %c", name));
        } else {
            self.emitln(fmt!("MOVE %c(PC),D0", name));
        }
    }

    priv fn factor(&mut self) {
        if self.look == '(' {
            self.match_('(');
            self.expression();
            self.match_(')');
        } else if self.is_alpha(self.look) {
            self.ident();
        } else {
            let num = self.get_num();
            // NOTE: bug here in rust compiler? When embedding the
            // self.get_num() call inside the fmt!() call we get 
            // an error about mutable loan pointer dereferencing
            self.emitln(fmt!("MOVE #%c,D0", num));
        }
    }

    priv fn multiply(&mut self) {
        self.match_('*');
        self.factor();
        self.emitln(~"MULS (SP)+,D0");
    }

    priv fn divide(&mut self) {
        self.match_('/');
        self.factor();
        self.emitln(~"MOVE (SP)+,D1");
        self.emitln(~"DIVS D1,D0");
    }

    priv fn term(&mut self) {
        self.factor();

        while self.is_mulop(self.look) {
            self.emitln(~"MOVE D0,-(SP)");

            match self.look {
                '*' => self.multiply(),
                '/' => self.divide(),
                  _ => self.expected(~"Mulop")
            };
        }
    }

    priv fn add(&mut self) {
        self.match_('+');
        self.term();
        self.emitln(~"ADD (SP)+,D0");
    }

    priv fn subtract(&mut self) {
        self.match_('-');
        self.term();
        self.emitln(~"SUB (SP)+,D0");
        self.emitln(~"NEG D0");
    }

    priv fn expression(&mut self) {
        if self.is_addop(self.look) {
            self.emitln(~"CLR D0");
        } else {
            self.term();
        }

        while self.is_addop(self.look) {
            self.emitln(~"MOVE D0,-(SP)");

            match self.look {
                '+' => self.add(),
                '-' => self.subtract(),
                  _ => self.expected(~"Addop")
            };
        }
    }

    priv fn assignment(&mut self) {
        let name = self.get_name();

        self.match_('=');
        self.expression();
        self.emitln(fmt!("LEA %c(PC),A0", name));
        self.emitln(~"MOVE D0,(A0)");
    }

    priv fn init(&mut self) {
        self.get_char();
    }

    fn parse(&mut self) {
        self.init();

        self.assignment();
    }
}

fn main() {
    let mut parser = ~Parser::new(io::stdin(), io::stdout());
    parser.parse();
}
