// taken from http://compilers.iecc.com/crenshaw/

// to do a set number of loops you can do
// for 5.times { do_some_stuff() }

extern mod std;

use core::io::{Reader, ReaderUtil, Writer};
use core::hashmap::HashMap;

pub struct Parser {
    reader: @Reader,
    writer: @Writer,
    look: char,
    table: ~HashMap<char, int>
}

impl Parser {
    fn new(reader: @Reader, writer: @Writer) -> Parser {
        Parser {
            reader: reader,
            writer: writer,
            look: ' ',
            table: ~HashMap::new()
        }
    }

    priv fn display<T: ToStr>(&mut self, v: T) {
        println(v.to_str());
    }

    priv fn get_char(&mut self) {
        self.look = self.reader.read_char();
    }

    priv fn error(&mut self, s: ~str) {
        io::println(~"Error: " + s);
    }

    priv fn abort(&mut self, s: ~str) {
        self.error(s);
        fail!();
    }

    priv fn expected(&mut self, s: ~str) {
        self.abort(s + ~" expected");
    }

    priv fn match_(&mut self, x: char) {
        if self.look == x { self.get_char(); }
        else { self.expected(fmt!("\"%?\"", x)); }
    }

    priv fn is_alpha(&mut self, c: char) -> bool {
        match c {
            'A'..'Z' => true,
            'a'..'z' => true,
            _        => false
        }
    }

    priv fn is_digit(&mut self, c: char) -> bool {
        char::is_digit(c)
    }

    priv fn is_addop(&mut self, c: char) -> bool {
        match c {
            '+' => true,
            '-' => true,
            _   => false
        }
    }

    priv fn is_mulop(&mut self, c: char) -> bool {
        match c {
            '*' => true,
            '/' => true,
            _   => false
        }
    }

    priv fn is_operator(&mut self, c: char) -> bool {
        self.is_addop(c) || self.is_mulop(c)
    }

    priv fn get_name(&mut self) -> char {
        if !self.is_alpha(self.look) { self.expected(~"Name"); }

        let name = self.look;
        self.get_char();
        name
    }

    priv fn get_num(&mut self) -> int {
        if !self.is_digit(self.look) { self.expected(~"Integer"); }

        let mut value = 0;

        while self.is_digit(self.look) {
            let n = int::from_str(str::from_char(self.look));

            value = value * 10 + match n { Some(n) => n, None => fail!(~"Malformed int") };
            self.get_char();
        }

        value
    }


    priv fn emit(&mut self, s: ~str) {
        self.writer.write_str(~"\t" + s);
    }

    priv fn emitln(&mut self, s: ~str) {
        self.writer.write_line(~"\t" + s);
    }

    priv fn factor(&mut self) -> int {
        let mut value = 0;

        if self.look == '(' {
            self.match_('(');
            value = self.expression();
            self.match_(')');
        } else if self.is_alpha(self.look) {
            self.display(self.is_alpha('2'));
            let found = self.table.find(&self.look);

            value = match found {
                Some(found) => *found,
                None        => fail!(fmt!("No name \"%?\" exists", self.look))
            };
        } else {
            value = self.get_num();
        }

        value
    }

    priv fn term(&mut self) -> int {
        let mut value = self.factor();

        while self.is_mulop(self.look) {
            match self.look {
                '*' => { self.match_('*'); value *= self.factor(); },
                '/' => { self.match_('/'); value /= self.factor(); }
                _   => self.expected(~"Mulop")
            };
        }

        value
    }

    priv fn expression(&mut self) -> int {
        let mut value = 0;

        if self.is_addop(self.look) {
            value = 0;
        } else {
            value = self.term();
        }

        while self.is_addop(self.look) {
            match self.look {
                '+' => { self.match_('+'); value += self.term(); },
                '-' => { self.match_('-'); value -= self.term(); },
                _   => { self.expected(~"Addop"); }
            };
        }

        value
    }

    priv fn assignment(&mut self) {
        let name = self.get_name();
        self.match_('=');

        let value = self.expression();
        self.table.insert(name, value);
    }

    priv fn init_table(&mut self) {
        // declare as ~str to avoid ambiguity with the compiler as
        // to whether the type is io or str (hence choosing the
        // correct each_char implementation) not sure why this is a problem
        let alphabet: ~str = ~"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for str::to_chars(alphabet).each |c| {
            self.table.insert(c.clone(), 0);
        }
    }

    priv fn init(&mut self) {
        self.init_table();

        self.get_char();
    }

    fn parse(&mut self) {
        self.init();

        let out = self.assignment();
        self.display(out);
    }
}

fn main() {
    let mut parser = ~Parser::new(io::stdin(), io::stdout());
    parser.parse();
}
